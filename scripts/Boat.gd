extends Spatial
class_name Boat

"""
Class that connects the visuals, input and simulation of a boat together
"""

signal interrupt_boarding

export(float) var max_single_row_distance = 0.4
export(NodePath) var dock_anchor_path
export(NodePath) var boat_anchor_path

onready var dock_anchors = get_node(dock_anchor_path).get_children()
onready var boat_anchors = get_node(boat_anchor_path).get_children()

onready var sim := get_node("Simulation") as BoatSim
onready var vis := get_node("Visuals") as BoatVisual
onready var input := get_node("InputHandler") as BoatInput

class Passenger:
	var money: int
	var visual: Spatial
	
	func _init(money: int, visual: Spatial):
		self.money = money
		self.visual = visual
		visual.set_coin_value(money)

var passengers_capacity := 6
var passengers = []

var minimum_weight_scale := 0.5
var movement_weight_scale := 1.0


func _process(delta: float):
	self.sim.update(delta * self.movement_weight_scale)


func compute_movement_weight_scale(num_passengers: int) -> float:
	var max_change_amount = 1.0 - minimum_weight_scale
	
	var passenger_change_amount = max_change_amount / float(passengers_capacity) 
	
	return 1.0 - (num_passengers * passenger_change_amount)

func board_passenger(passenger: Passenger):
	self.passengers.push_back(passenger)
	passenger.visual.travel_to(boat_anchors[self.passengers.size() -1].global_transform.origin, 0.3)
	self.movement_weight_scale = compute_movement_weight_scale(self.passengers.size())
	return passenger


func unboard_passenger():
	var passenger = passengers.pop_front()
	passengers.erase(passenger)
	if passenger:
		passenger.visual.queue_free()
	self.movement_weight_scale = compute_movement_weight_scale(self.passengers.size())
	return passenger


func is_full():
	return passengers.size() == passengers_capacity
	
func is_empty():
	return passengers.size() == 0 

func set_input_enabled(ena):
	self.input.enabled = ena


func _on_boat_arrived():
	"""
	Reset the simulation.
	"""
	
	self.sim.reset()
	
	self.vis.turn()


func _on_boat_moved(amount: float):
	"""
	Update the visuals of a given boat
	"""
	self.vis.advance_pos(amount)
	for i in range(passengers.size()):
		passengers[i].visual.global_transform.origin = boat_anchors[i].global_transform.origin


func _on_boat_stopped():
	"""
	Prompt user for a new charge
	"""
	self.input.set_chargable()

func _on_InputHandler_charge_finished(charge_amount: float):
	"""
	Apply the charge to the simulation
	"""
	self.sim.apply_charge(charge_amount * max_single_row_distance)


func _on_finish_boarding():
	emit_signal("interrupt_boarding")
	pass
	#TODO this can be called both while a passenger is boarding and when a passenger is not boarding