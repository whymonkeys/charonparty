extends Node
class_name BoatInput

"""
Input state machine for boat interactions
"""

signal charge_started()
signal charge_finished(charge_amount)
signal finish_boarding()

export var action: String = ""

# Time factor for the charge.
# For example 1/2 takes 2 seconds to charge from 0 to 1
var charge_time_scale := 2.0

# When disabled don't process anything
export var enabled: bool = true

enum State {
	Charging,
	NotCharging,
	Waiting,
	Boarding,
}

var state: int = State.NotCharging


export var charge: float = 0.0

func set_chargable():
	self.enabled = true

func _process(time: float):
	if !self.enabled:
		return
	
	var pressed := Input.is_action_just_pressed(self.action)
	
	match state:
		State.NotCharging:
			if pressed:
				self.emit_signal("charge_started")
				self.state = State.Charging
				self.charge = rand_range(0.0, 1.0)

		State.Charging:
			var accumulation_done := false
			
			if pressed:
				accumulation_done = true
			else:
				# accumulate charge
				self.charge += time * self.charge_time_scale
				
				# accumulated enough
				if self.charge >= 1.0 or self.charge <= 0.0:
					self.charge_time_scale =  -self.charge_time_scale
					self.charge = clamp( self.charge, 0.0, 1.0)
			
			if accumulation_done:
				self.emit_signal("charge_finished", self.charge)
				self.enabled = false
				self.charge = 0.0
				self.state = State.NotCharging
