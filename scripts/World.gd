extends Spatial

export(PackedScene) var soul_scene

onready var boats := [
	$Dynamic/Boats/Boat0,
	$Dynamic/Boats/Boat1,
]

onready var load_timers := [
	$LogicHelpers/Timer0,
	$LogicHelpers/Timer1,
]

onready var game_timer := $Timer
onready var ui = $Control

# load and unload time of passengers in seconds
const LOAD_TIME: float = 0.5

var queue := [
	[],
	[],
]
var coins = [0,0]

enum BoatState {
	Empty,
	Full,
	Loading,
	Unloading,
}

var boat_states := [BoatState.Empty, BoatState.Empty]

func _ready():
	
	for i in range(boats.size()):
		coins[i] = 0
		boats[i].sim.connect("arrived", self, "_on_boat_arrived", [i])
		boats[i].sim.connect("moved", self, "_on_boat_moved", [i])
		for j in range(boats[i].dock_anchors.size()):
			var p = generate_passenger(i)
			p.visual.global_transform.origin = boats[i].dock_anchors[j].global_transform.origin
			queue[i].append(p)
		load_timers[i].wait_time = LOAD_TIME
		load_timers[i].connect("timeout", self, "_on_load_timer_timeout", [i])
		
		ui.set_player_coins(i, 0)
	
	game_timer.start()



func _process(delta: float):
	ui.change_time(game_timer.time_left)


	
func _on_boat_arrived(boat_idx: int):
	print("Boat ", boat_idx, " arrived")
	
	var new_state
	
	if boat_states[boat_idx] == BoatState.Empty:
		new_state = BoatState.Loading
		boats[boat_idx].set_input_enabled(true)
	elif boat_states[boat_idx] == BoatState.Full:
		boats[boat_idx].set_input_enabled(false)
		new_state = BoatState.Unloading
	else:
		# should be either empty or full, nothing in between
		assert false
		pass
	
	boat_states[boat_idx] = new_state
	
	# start load/unload
	load_timers[boat_idx].start()


func _on_load_timer_timeout(boat_idx: int):
	if boat_states[boat_idx] == BoatState.Loading:
		boats[boat_idx].board_passenger(remove_passenger(boat_idx))
		if boats[boat_idx].is_full():
			boat_states[boat_idx] = BoatState.Full
		pass
	
	if boat_states[boat_idx] == BoatState.Unloading:
		var passenger = boats[boat_idx].unboard_passenger()
		if passenger != null:
			coins[boat_idx] += passenger.money
		ui.set_player_coins(boat_idx, coins[boat_idx])
		if boats[boat_idx].is_empty():
			boat_states[boat_idx] = BoatState.Empty
			boats[boat_idx].set_input_enabled(true)



func _on_load_stop(boat_idx: int):
	boat_states[boat_idx] = BoatState.Full
	load_timers[boat_idx].stop()

func _on_boat_moved(amount, boat_idx):
	if boat_states[boat_idx] == BoatState.Loading:
		boat_states[boat_idx] = BoatState.Full


func remove_passenger(boat_idx):
	print("queue before", queue[boat_idx].size())
	var passenger = queue[boat_idx].pop_front()
	print("queue after", queue[boat_idx].size())
	queue[boat_idx].append(generate_passenger(boat_idx))
	print("queue after aafter", queue[boat_idx].size())
	advance_souls(boat_idx)
	return passenger


func generate_passenger(boat_idx):
	var soul = soul_scene.instance()
	add_child(soul)
	var passenger = Boat.Passenger.new(int(rand_range(1, 4.3)), soul)
	passenger.visual.global_transform.origin = Vector3(100.0, 0.0, 0.0)
	return passenger

func advance_souls(player_idx):
	for j in range(queue[player_idx].size()):
		var p = queue[player_idx][j]
		p.visual.travel_to(boats[player_idx].dock_anchors[j].global_transform.origin, 0.1)

func _on_Timer_timeout():
	"""
	Fires when the time ran out.
	"""
	if coins[0] > coins[1]:
		ui.show_winner(0)
	else:
		ui.show_winner(1)
	# TODO for Ila
	# Display and do the reset :)