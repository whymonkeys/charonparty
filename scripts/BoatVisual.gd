extends Spatial
class_name BoatVisual

"""
Handles the visual/node representation of a boat
"""

var soul_anchors = []

const TRACK_LENGTH: float = 18.0

var direction: int = -1

func _ready():
	self.transform.origin.z = -float(TRACK_LENGTH) / 2.0
	turn()

func turn():
	self.global_transform.basis = global_transform.basis.rotated( Vector3.UP, PI)
	self.direction = -self.direction

func advance_pos(amount: float):
	self.transform.origin.z += self.direction * amount * TRACK_LENGTH
