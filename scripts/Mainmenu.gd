extends Control


func _on_Quit_pressed():
	get_tree().quit()


func _on_Credits_pressed():
	pass # Replace with function body.


func _on_Options_pressed():
	pass # Replace with function body.


func _on_Play_pressed():
	# TODO depending on the complexity, we might want to not change scene like this, but have a persistent node instead
	get_tree().change_scene("res://scenes/World.tscn")
	pass # Replace with function body.
