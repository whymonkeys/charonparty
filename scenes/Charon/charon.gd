extends Spatial

export(Color) var player_color = Color.rebeccapurple

func _ready():
	var mesh: MeshInstance = $Armature/Skeleton/Sphere
	var mat: SpatialMaterial = $Armature/Skeleton/Sphere.get_surface_material(1)
	for i in range(3):
		print(mesh.get_surface_material(i))
	mat = mat.duplicate()
	mat.albedo_color = player_color
	mat.emission = player_color
	$Armature/Skeleton/Sphere.set_surface_material(1, mat)

func set_charge_amount(amount: float):
	if amount > 0.7:
		$AnimationPlayer.play("Row")
	elif amount > 0.3:
		$AnimationPlayer.play("MidPowerRow")
	else:
		if global_transform.basis.x.dot(Vector3.FORWARD) < 0.0:
			$AnimationPlayer.play("ZeroPower")
		else:
			$AnimationPlayer.play("ZeroPower_other")