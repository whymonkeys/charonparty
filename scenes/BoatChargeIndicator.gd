extends Spatial

export(NodePath) var input_path
onready var input_node = get_node(input_path)

func _ready():
	_charge_finished(0.0)
	input_node.connect("charge_finished", self, "_charge_finished")
	input_node.connect("charge_started", self, "_start_charge")

func set_current_charge(charge: float):
	$AnimationPlayer.play("default")
	$AnimationPlayer.seek(charge * $AnimationPlayer.current_animation_length)


func _start_charge():
	set_process(true)
	visible = true


func _process(delta):
	set_current_charge(input_node.charge)


func _charge_finished(charge_amount):
	set_process(false)
	visible = false
