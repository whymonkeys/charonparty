extends Spatial

export(float) var coin_offset = 0.3

var mat = preload("res://scenes/Soul/SoulMaterial.tres")
var coin_scene = preload("res://scenes/Soul/Coin.tscn")

var _prev_pos
onready var position_tween = $Tween

var _wanted_pos = null

func _ready():
	_prev_pos = global_transform.origin
	mat = mat.duplicate()
	$Cube.set_surface_material(0,mat)

func travel_to(target_position: Vector3, time: float):
	#global_transform.origin = target_position
	#return # Tween is not working :/
	position_tween.interpolate_property(
		self,
		"global_transform:origin",
		global_transform.origin,
		target_position,
		time,
		Tween.TRANS_SINE,
		Tween.EASE_OUT_IN
		)
	position_tween.start()
	yield(position_tween, "tween_completed")
	_wanted_pos = null

func _process(delta):
	mat.set_shader_param("velocity_delta", _prev_pos - global_transform.origin)
	_prev_pos = global_transform.origin
	if _wanted_pos != null:
		global_transform.origin = _wanted_pos


func set_coin_value(value: int):
	for c in $CoinPositions.get_children():
		$CoinPositions.remove_child(c)
		c.queue_free()
	for i in range(value):
		var coin = coin_scene.instance()
		$CoinPositions.add_child(coin)
		coin.transform.origin += Vector3(0.0, i * coin_offset, 0.0)