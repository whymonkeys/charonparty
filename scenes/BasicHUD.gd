extends Control

func set_player_coins(player_idx, coins):
	get_child(player_idx).get_node("Label").text = "Coins: " + str(coins)

func change_time(time: float):
	$Label.text = "Time left: " + str(int(time + 1))

func show_winner(idx):
	var player_name
	if idx == 0:
		player_name = "green"
	else:
		player_name = "pink"
	$AcceptDialog.dialog_text = "Player " + player_name + " won!"
	$AcceptDialog.popup_centered()



func _on_AcceptDialog_confirmed():
	get_tree().change_scene("res://scenes/World.tscn")
