extends Node
class_name BoatSim

"""
Simulation of a boat advancing forwards
"""

# amount: float
signal moved(amount)
signal stopped()

signal arrived()

# The maximum time the boat is in the "moving" state while
# processing a charge.
const MAX_CHARGE_TIME: float = 3.0

enum State {
	# Waiting to start the journey
	Boarding,
	# Arrived on the other side
	Arrived,
	# In the process of moving forward
	Moving,
	# On the journey but currently not moving
	Halted,
}

var state: int = State.Boarding

# normalized position of the boat.
# The boat *model* does not care which way it moves,
# only that it moves from one shore to another.
var position: float = 0.0

var charge: float = 0.0

func _init() -> void:
	self.reset()

func reset() -> void:
	"""
	Reset the simulation state.
	"""
	self.position = 0.0
	self.charge = 0.0
	self.state = State.Boarding

func apply_charge(new_charge: float) -> void:
	"""
	Makes the boat go wheeey by applying the charge given from the user.
	
	A charge of value 1.0 will move the boat from one side to the other
	in one go.
	"""
	
	assert new_charge >= 0.0
	assert new_charge <= 1.0
	assert self.state != State.Moving
	assert self.charge == 0.0
	
	self.state = State.Moving
	self.charge = new_charge
	
	assert self.state == State.Moving
	assert self.charge >= 0.0


func update(time_delta: float):
	"""
	Update the state of the boat
	"""
	
	match self.state:
		State.Boarding:
			# Do nothing.
			pass
		State.Arrived:
			# Do nothing.
			pass
		State.Halted:
			# Do nothing.
			pass
		State.Moving:
			
			var charge_to_remove := time_delta / MAX_CHARGE_TIME
			
			var went_out_of_charge := self.charge < charge_to_remove
			
			if went_out_of_charge:
				charge_to_remove = self.charge
			
			# TODO replace with something smoother / less linear
			var attempted_position_progress := charge_to_remove
			
			var reached_end := (self.position + attempted_position_progress) >= 1.0
			
			# actual way travelled
			var position_progress: float
			
			if reached_end:
				position_progress = 1.0 - self.position
			else:
				position_progress = attempted_position_progress
			
			#
			# update state
			#
			self.position = self.position + position_progress
			self.charge = self.charge - charge_to_remove
			
			if reached_end:
				self.state = State.Arrived
			elif went_out_of_charge:
				self.state = State.Halted
			
			#
			# notify listeners
			#
			self.emit_signal("moved", position_progress)
			
			if reached_end:
				self.emit_signal("arrived")
				
			elif went_out_of_charge:
				self.emit_signal("stopped")
