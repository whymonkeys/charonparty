Code files are released under the MIT license (see [license file](LICENSE_MIT)).

Assets are released under CC0.